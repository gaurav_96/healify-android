package com.healify.www.healify;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.healify.www.healify.home.HomeNetworkService;
import com.healify.www.healify.home.HomeRvAdapter;
import com.healify.www.healify.home.MedicalCondition;
import com.healify.www.healify.utils.CommonUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private HomeRvAdapter rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpView();
        setUpRecyclerView();
        fetchData();
    }

    private void setUpView(){
        int width = (CommonUtils.getScreenWidth(this) * 6) / 10;
        int height = CommonUtils.getScreenHeight(this) / 3;
        ImageView imageView = findViewById(R.id.image);
        CommonUtils.setViewDimens(imageView,height,width);
        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchData();
            }
        });
    }

    private void setUpRecyclerView(){
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        rvAdapter = new HomeRvAdapter();
        rvAdapter.setInteractionListener(new HomeRvAdapter.InteractionListener() {
            @Override
            public void onItemClicked(MedicalCondition medicalCondition, ImageView sharedImageView) {
                Intent i = new Intent(MainActivity.this,MedicalConditionActivity.class);
                i.putExtra(MedicalConditionActivity.MEDICAL_CONDITION_BK,medicalCondition);
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,sharedImageView,"home_transition");
                startActivity(i,optionsCompat.toBundle());
            }
        });
        recyclerView.setAdapter(rvAdapter);
    }

    private void fetchData(){
        toggleProgressVisibility(true);
        toggleRetryVisibility(false);
        CommonUtils
            .getRetrofit()
            .create(HomeNetworkService.class)
            .getAllMedicalConditions()
            .enqueue(new Callback<List<MedicalCondition>>() {
            @Override
            public void onResponse(Call<List<MedicalCondition>> call, Response<List<MedicalCondition>> response) {
                toggleProgressVisibility(false);
                if(response.isSuccessful() && response.body() != null){
                    setDataToRv(response.body());
                }else{
                    onFailure(call,new Throwable("Something Went Wrong"));
                }
            }

            @Override
            public void onFailure(Call<List<MedicalCondition>> call, Throwable t) {
                toggleProgressVisibility(false);
                toggleRetryVisibility(true);
                Toast.makeText(MainActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDataToRv(List<MedicalCondition> data){
        if(data != null && data.size() > 0){
            rvAdapter.setDataList(data);
        }else{
            toggleRetryVisibility(true);
        }
    }

    private void toggleProgressVisibility(boolean show){
        int visibility = show ? View.VISIBLE : View.GONE;
        findViewById(R.id.progress_bar).setVisibility(visibility);
    }

    private void toggleRetryVisibility(boolean show){
        int visibility = show ? View.VISIBLE : View.GONE;
        findViewById(R.id.retry_button).setVisibility(visibility);
    }

}
