package com.healify.www.healify.medicalConditionDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DoDont implements Serializable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("is_do")
    @Expose
    private int isDo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getIsDo() {
        return isDo != 0;
    }

    public void setIsDo(int isDo) {
        this.isDo = isDo;
    }
}
