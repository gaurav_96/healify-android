package com.healify.www.healify.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.healify.www.healify.R;

import java.util.List;

public class HomeRvAdapter extends RecyclerView.Adapter<HomeRvViewHolder> {

    private List<MedicalCondition> dataList;
    private InteractionListener interactionListener;

    @NonNull
    @Override
    public HomeRvViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_rv_item,parent,false);
        final HomeRvViewHolder viewHolder = new HomeRvViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = viewHolder.getAdapterPosition();
                if(interactionListener != null && position != RecyclerView.NO_POSITION){
                    interactionListener.onItemClicked(dataList.get(position), (ImageView)viewHolder.itemView.findViewById(R.id.image_view));
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeRvViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    public void setDataList(List<MedicalCondition> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public void setInteractionListener(InteractionListener interactionListener) {
        this.interactionListener = interactionListener;
    }

    public interface InteractionListener{
        void onItemClicked(MedicalCondition medicalCondition, ImageView sharedImageView);
    }

}
