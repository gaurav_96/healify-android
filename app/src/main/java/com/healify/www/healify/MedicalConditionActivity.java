package com.healify.www.healify;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.healify.www.healify.home.MedicalCondition;
import com.healify.www.healify.medicalConditionDetails.DoDont;
import com.healify.www.healify.medicalConditionDetails.MedicalConditionNetworkService;
import com.healify.www.healify.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicalConditionActivity extends AppCompatActivity {

    public static final String MEDICAL_CONDITION_BK = "MEDICAL_CONDITION_BK";
    private MedicalCondition medicalCondition;
    private ImageView headerImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_condition);
        medicalCondition = (MedicalCondition) getIntent().getExtras().getSerializable(MEDICAL_CONDITION_BK);
        setUpView();
        setUpDescription();
        fetchDosAndDonts();
    }

    private void setUpView(){
        getSupportActionBar().setTitle(medicalCondition.getName());
        headerImage = findViewById(R.id.image_view);
        int width = CommonUtils.getScreenWidth(this) / 2;
        int height = CommonUtils.getScreenHeight(this) / 4;
        CommonUtils.setViewDimens(headerImage,height,width);
        Glide.with(this).load(medicalCondition.getImageUrl()).into(headerImage);
        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchDosAndDonts();
            }
        });
    }

    private void setUpDescription() {
        if (medicalCondition.getDescription() != null && medicalCondition.getDescription().trim().length() > 0) {
            ((TextView) findViewById(R.id.subtext)).setText(medicalCondition.getDescription());
        } else {
            findViewById(R.id.description_header).setVisibility(View.GONE);
            findViewById(R.id.description_container).setVisibility(View.GONE);
        }
    }

    private void fetchDosAndDonts(){
        toggleProgressVisibility(true);
        toggleRetryVisibility(false);
        CommonUtils
            .getRetrofit()
            .create(MedicalConditionNetworkService.class)
            .getDosAndDontsForMedicalCondition(medicalCondition.getId())
            .enqueue(new Callback<List<DoDont>>() {
            @Override
            public void onResponse(Call<List<DoDont>> call, Response<List<DoDont>> response) {
                if(response.isSuccessful() && response.body() != null){
                    toggleProgressVisibility(false);
                    setDosAndDontsData(response.body());
                }else{
                    onFailure(call, new Throwable("Something went wrong"));
                }
            }

            @Override
            public void onFailure(Call<List<DoDont>> call, Throwable t) {
                toggleProgressVisibility(false);
                toggleRetryVisibility(true);
                Toast.makeText(MedicalConditionActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDosAndDontsData(List<DoDont> data){
        List<DoDont> dosList = new ArrayList<>();
        List<DoDont> dontsList = new ArrayList<>();
        for(DoDont obj : data){
            if(obj.getIsDo()){
                dosList.add(obj);
            }else{
                dontsList.add(obj);
            }
        }
        renderDos(dosList);
        renderDonts(dontsList);
    }

    private void renderDos(List<DoDont> dos){
        if(dos != null && dos.size() > 0){
            findViewById(R.id.dos_header).setVisibility(View.VISIBLE);
            findViewById(R.id.dos_container).setVisibility(View.VISIBLE);
            for(DoDont obj : dos){
                if(obj.getText() != null && obj.getText().trim().length() > 0) {
                    View view = LayoutInflater.from(this).inflate(R.layout.subtext_item, (ViewGroup) findViewById(R.id.dos_container), false);
                    ((TextView) view.findViewById(R.id.subtext)).setText(obj.getText());
                    ((ViewGroup) findViewById(R.id.dos_container)).addView(view);
                }
            }
        }
    }

    private void renderDonts(List<DoDont> donts){
        if(donts != null && donts.size() > 0){
            findViewById(R.id.donts_header).setVisibility(View.VISIBLE);
            findViewById(R.id.donts_container).setVisibility(View.VISIBLE);
            for(DoDont obj : donts){
                if(obj.getText() != null && obj.getText().trim().length() > 0) {
                    View view = LayoutInflater.from(this).inflate(R.layout.subtext_item, (ViewGroup) findViewById(R.id.donts_container), false);
                    ((TextView) view.findViewById(R.id.subtext)).setText(obj.getText());
                    ((ViewGroup) findViewById(R.id.donts_container)).addView(view);
                }
            }
        }
    }

    private void toggleProgressVisibility(boolean show){
        int visibility = show ? View.VISIBLE : View.GONE;
        findViewById(R.id.progress_bar).setVisibility(visibility);
    }

    private void toggleRetryVisibility(boolean show){
        int visibility = show ? View.VISIBLE : View.GONE;
        findViewById(R.id.retry_button).setVisibility(visibility);
    }

}
