package com.healify.www.healify.medicalConditionDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MedicalConditionNetworkService {

    @GET("dos/{mc_id}")
    Call<List<DoDont>> getDosAndDontsForMedicalCondition(@Path("mc_id") int medicalConditionId);

}
