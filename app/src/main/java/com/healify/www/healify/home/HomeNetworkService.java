package com.healify.www.healify.home;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HomeNetworkService {

    @GET("medicalCondition")
    Call<List<MedicalCondition>> getAllMedicalConditions();

}
