package com.healify.www.healify.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommonUtils {

    private CommonUtils(){}

    public static Retrofit getRetrofit(){
        return new Retrofit.Builder()
            .baseUrl("http://192.168.43.92:8000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    }

    public static int getScreenWidth(Context context){
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight(Context context){
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static void setViewDimens(View view, int height, int width){
        if(view != null){
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if(layoutParams == null){
                layoutParams = new ViewGroup.LayoutParams(width,height);
            }else{
                layoutParams.height = height;
                layoutParams.width = width;
            }
            view.setLayoutParams(layoutParams);
        }
    }

}
